package co.com.segurosalfa.siniestros.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class SnrCausalAprobacionDTO {

	private Long codigo;
	private String nombre;
}
