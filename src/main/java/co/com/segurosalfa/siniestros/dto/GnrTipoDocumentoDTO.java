package co.com.segurosalfa.siniestros.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class GnrTipoDocumentoDTO {

	private Integer id;
	private String nombre;
}
