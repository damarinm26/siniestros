package co.com.segurosalfa.siniestros.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class FiltroRangoDTO {

	private Long datoInicio;
	private Long datoFinal;

	
}
