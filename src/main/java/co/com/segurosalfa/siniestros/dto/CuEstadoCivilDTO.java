package co.com.segurosalfa.siniestros.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class CuEstadoCivilDTO {
	private Integer id;
	private String nombre;
	private String abreviatura;
}
