package co.com.segurosalfa.siniestros.repo;

import org.springframework.stereotype.Repository;

import co.com.segurosalfa.siniestros.entity.SnrOrigen;

@Repository
public interface ISnrOrigenesRepo extends IGenericRepo<SnrOrigen, Integer>{

}