package co.com.segurosalfa.siniestros.repo;

import co.com.segurosalfa.siniestros.entity.SnrCausalAprobacion;

public interface ISnrCausalAprobacionRepo extends IGenericRepo<SnrCausalAprobacion, Long> {

}
