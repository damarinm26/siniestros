package co.com.segurosalfa.siniestros.repo;

import co.com.segurosalfa.siniestros.entity.SnrTipoEquivalencia;

public interface ISnrTipoEquivalenciaRepo extends IGenericRepo<SnrTipoEquivalencia, Long>{

}
