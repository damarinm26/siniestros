package co.com.segurosalfa.siniestros.repo;

import org.springframework.stereotype.Repository;

import co.com.segurosalfa.siniestros.entity.SnrCausaSiniestro;

@Repository
public interface ISnrCausaSiniestroRepo extends IGenericRepo<SnrCausaSiniestro, Integer>{

}