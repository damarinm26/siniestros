package co.com.segurosalfa.siniestros.repo;

import co.com.segurosalfa.siniestros.entity.SnrAbogado;

public interface ISnrAbogadoRepo extends IGenericRepo<SnrAbogado, Integer> {


}
