package co.com.segurosalfa.siniestros.exception;

public class SiprenException extends Exception {

	private static final long serialVersionUID = 2686160723786573260L;

	public SiprenException(String mensaje) {
		super(mensaje);
	}
}
