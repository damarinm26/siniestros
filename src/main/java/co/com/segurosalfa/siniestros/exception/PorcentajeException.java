package co.com.segurosalfa.siniestros.exception;

public class PorcentajeException extends Exception {

	private static final long serialVersionUID = -785351525232460126L;

	public PorcentajeException(String mensaje) {
		super(mensaje);
	}
}
