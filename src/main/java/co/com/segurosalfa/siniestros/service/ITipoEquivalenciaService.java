package co.com.segurosalfa.siniestros.service;

import co.com.segurosalfa.siniestros.entity.SnrTipoEquivalencia;

public interface ITipoEquivalenciaService extends ICRUD<SnrTipoEquivalencia, Long>{

}
